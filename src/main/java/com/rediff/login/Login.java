package com.rediff.login;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.rediff.util.Common;

public class Login extends Common {

//Comment by computer2
	@BeforeTest
	public void test1() throws IOException {
		loadPropertyFiles();
		log.debug("in before test");
	}

	@AfterTest
	public void test2() {
		log.debug("in after test");
		closePropertiesFilesConnections();
	}

	@BeforeMethod
	public void openBrowser() {
		openBrowsers();
	}

	@Test(dataProvider = "testData")
	public void invalidLogin(String usename, String password) throws IOException {
		try {
			driver.findElement(By.id(element.getProperty("userNameInput"))).sendKeys(usename);
			driver.findElement(By.id(element.getProperty("passwordInput"))).sendKeys(password);
			driver.findElement(By.name(element.getProperty("signInButton"))).click();

			String actualText = driver.findElement(By.xpath(element.getProperty("invalidLoginErrorMessage"))).getText();
			String expectedText = "Wrong username and password combination.";

			Assert.assertEquals(actualText, expectedText);
			log.debug("in actual Test with test data: " + usename + "--" + password);
		} catch (Exception e) {
			captureScreenshots();
		}

	}

	@AfterMethod
	public void closeBrowser() {
		closeBrowsers();
	}

	@DataProvider
	public Object[][] testData() throws EncryptedDocumentException, IOException {

		return readExcel();
	}

}
