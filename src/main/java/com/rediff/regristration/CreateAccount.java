package com.rediff.regristration;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.rediff.util.Common;

public class CreateAccount extends Common {

	@BeforeTest
	public void openBrowser() throws IOException {
		loadPropertyFiles();
		openBrowsers();
	}

	@Test(priority = 1)
	public void createAccount() throws IOException {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 30);

			wait.until(ExpectedConditions
					.visibilityOf(driver.findElement(By.linkText(element.getProperty("createNewAccountLink")))))
					.click();

			wait.until(ExpectedConditions
					.visibilityOf(driver.findElement(By.xpath(element.getProperty("firstNameInput")))))
					.sendKeys("Manidhar");

			driver.findElement(By.xpath(element.getProperty("emailInput"))).sendKeys("mandihar");

			driver.findElement(By.xpath(element.getProperty("homeLink"))).click();

			driver.findElement(By.linkText(element.getProperty("signInLinkText"))).click();
		} catch (Exception e) {
			captureScreenshots();
			e.printStackTrace();
		}

	}

	@Test(priority = 2, dependsOnMethods = "createAccount")
	public void login() throws IOException {
		try {
			driver.findElement(By.id(element.getProperty("userNameInput"))).sendKeys("mandihar");
			driver.findElement(By.id(element.getProperty("passwordInput"))).sendKeys("password@1234");
			driver.findElement(By.name(element.getProperty("signInButton"))).click();
		} catch (Exception e) {
			captureScreenshots();
			e.printStackTrace();
		}
	}

	@AfterTest
	public void closeBrowser() {
		closeBrowsers();
		closePropertiesFilesConnections();
	}

}
